# contractor-onboarding

Puzzl onboarding for React Native

## Installation

```sh
npm install puzzl-contractor-onboarding
or
yarn add puzzl-contractor-onboarding
```
```
#### iOS
* Make your app target iOS 11.0 or higher by adding this line to your Podfile: `platform :ios, '11.0'`
* Include Swift code in your project.
If your project does not already include Swift code, then create a new empty Swift file in the base of your project using Xcode and add the bridging header if Xcode offers.
* Add the following to your Info.plist:
```
<dict>
...
    <key>NSCameraUsageDescription</key>
    <string>Access to camera is needed for user identification purposes</string>
    <key>NSMicrophoneUsageDescription</key>
    <string>Access to microphone is needed for video identification</string>
</dict>
```

#### Android
* Add a new maven destination to the repositories in the `allprojects` section of `build.gradle`:
```
allprojects {
    repositories {
        // ... local react native repos
        maven { url "https://cdn.veriff.me/android/" }
        google()
        jcenter()
    }
}
```

## Usage

This library exports a single component called PuzzlOnboarding with the following props:

```ts
interface OnboardingProps {
  APIKey: string;
  companyID: string;
  employeeID?: string; // (optional) employeeID or contractorID is required
  contractorID?: string; // (optional) employeeID or contractorID is required
  onCancel: () => void | Promise<void>;
  onFinished: () => void | Promise<void>;
  onError?: (error) => any; // (optional; falls back to onCancel)
  showError?: boolean; // (optional; default: true) Allow the component to display its own error message before cancelling onboarding
  errorMessage?: string; // (optional) Personalize the error message
  profile?: bool; // toggle profile info, defaults to true
  contractor_acct?: bool; // toggle add contractor account, defaults to true
  
}
```

```jsx
import PuzzlOnboarding from "puzzl-contractor-onboarding";

// ...

<PuzzlOnboarding
  companyID="..."
  contractorID="..."
  APIKey="..."
  onCancel={() => hide()}
  onError={(error) => {console.log(error)}}
  onFinished={() => {
    // Onboarding completed successfully!
    hide()
  }}
  profile={true}
  contractor_acct={true}
  
/>
```
