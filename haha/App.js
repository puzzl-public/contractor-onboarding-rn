import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity,
  Modal,
  Alert,} from 'react-native';
import PuzzlOnboarding from "@puzzlpayroll/contractor-onboarding";


export default function App() {
  const [showOnboarding, setOnboarding] = React.useState(false);

  const handleCancel = () => { 
    console.log("on cancel")
    setOnboarding(false)};

  const handleFinished = () => {
    console.log("on finished")
    Alert.alert(
      "Success",
      "Puzzl onboarding complete!",
      [
        {
          text: "OK",
          onPress: () => setOnboarding(false),
        },
      ],
      { cancelable: false }
    ) };
    
    const handleError = () => {
      console.log("on finished")
      Alert.alert(
        "Success",
        "Puzzl onboarding complete!",
        [
          {
            text: "OK",
            onPress: () => setOnboarding(false),
          },
        ],
        { cancelable: false }
      ) };

  return (

    <View style={styles.container}>
      <StatusBar style="dark" />
      <Text style={styles.instructions}>Register with Puzzl to get paid!</Text>

      <TouchableOpacity
        onPress={() => setOnboarding(true)}
        style={styles.button}
      >
        <Text style={styles.buttonText}>Onboard with Puzzl</Text>
      </TouchableOpacity>
      <Modal
        animationType={"slide"}
        visible={showOnboarding}
        onRequestClose={() => setOnboarding(false)}
        transparent
        fullScreen={false}
      >
              {/* <PuzzlOnboarding
          companyID="fc235f012bae46aa8a082f357715bcfa"
          APIKey="4ceeb4f720664d6aa190b2c39179894b"
          employeeID="5ffa35d96006349347d2455f"
          onCancel={handleCancel}
          onFinished={handleFinished}
          profile={true}
          employee_acct={true}
          id_scan={true}
          i9_form={false}
          

        /> */}

{/*         
        <PuzzlOnboarding
          companyID="fc235f012bae46aa8a082f357715bcfa"
          APIKey="0c9c92d193654e95b06a21ccb33aa8b1"
          employeeID="5ffa35d96006349347d2455f"
          onCancel={handleCancel}
          onFinished={handleFinished}
          profile={true}
          employee_acct={true}
          id_scan={true}
          i9_form={false}
          

        /> */}
   <PuzzlOnboarding
          companyID="fc235f012bae46aa8a082f357715bcfa"
          APIKey="b8c681c8f7c14ed8ab2cf32d6db811ae"
          contractorID="5ffbe28e7e659843ba238643"
          onCancel={handleCancel}
          onFinished={handleFinished}
          onError={(error) => console.log(error)}
          profile={true}
          contractor_acct={true}
          
          

        />
        
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 305,
    height: 159,
    marginBottom: 20,
    resizeMode: "contain",
  },
  instructions: {
    color: "#888",
    fontSize: 18,
    marginHorizontal: 15,
    marginBottom: 10,
  },
  button: {
    marginTop: 70,
    backgroundColor: "#0E64DC",
    borderRadius: 5,
    paddingTop: 15,
    paddingBottom: 15,
    paddingRight: 45,
    paddingLeft: 45,
  },
  buttonText: {
    color: "#fff",
    fontSize: 22,
  },
});



