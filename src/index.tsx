import "react-native-gesture-handler";
import * as React from "react";
import { Alert, StyleSheet, Image, TouchableOpacity, View } from "react-native";
import {
  SafeAreaProvider,
  SafeAreaView,
  SafeAreaInsetsContext,
} from "react-native-safe-area-context";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Intro from "./screens/Intro";

import ContractorProfileInfo from "./screens/ContractorProfileInfo";
import CreateAccount from "./screens/CreateAccount";
// import Veriff from "./screens/Veriff";
import HelloSign from "./screens/HelloSign";
import ThankYou from "./screens/ThankYou";
import LoadingScreen from "./components/Loading";
import ExitDialog from "./components/ExitDialog";
import TestModeBanner from "./components/TestModeBanner";
import { getContractorInfo, getUserInfo } from "./functions/api";
import ErrorContext from "./context/ErrorContext";
import type {
  GetUserInfoResponseType,
  
  GetContractorInfoResponseType,
} from "./types/api";
import type { RootStackParamList } from "./types/navigation";
import close_icon from "./assets/images/close_icon.png";

const Stack = createStackNavigator<RootStackParamList>();

interface OnboardingProps {
  APIKey: string;
  companyID: string;
  contractorID: string;
  onCancel: () => void | Promise<void>;
  onFinished: () => void | Promise<void>;
  onError?: (error:any) => void | Promise<void>;
  profile?:boolean,
  contractor_acct?:boolean,
  showError?: boolean;
  errorMessage?: string;
}

const PuzzlOnboarding: React.FC<OnboardingProps> = ({
  APIKey,
  companyID,
  contractorID,
  onCancel,
  onFinished,
  onError=(error:any) => console.log(error),
  profile=true,
  contractor_acct=true,
  showError = true,
  errorMessage = "An error occurred, please try again later.",
}) => {
  const [loading, setLoading] = React.useState(true);
  const [confirmExit, showConfirmExit] = React.useState(false);
  const [userInfo, setUserInfo] = React.useState<
    GetUserInfoResponseType["data"] | null
  >(null);
 
  const [contractorInfo, setContractorInfo] = React.useState<
    GetContractorInfoResponseType["data"] | null
  >(null);
  const [testMode, setTestMode] = React.useState<boolean>(false);
  const [controlsHidden, setControlsHidden] = React.useState<boolean>(false);
  const isEmpty = (str: string): boolean => str == null || str.length === 0;
  React.useEffect(() => {
    (async () => {
      try {
        if (APIKey == null || APIKey.length === 0) {
          throw new Error("The APIKey prop is not set!");
        } else if (isEmpty(companyID)) {
          throw new Error("The companyID prop is not set!");
        } else if (isEmpty(contractorID)) {
          throw new Error("The employeeID prop is not set!");
        }

  

        const userResp = await getUserInfo(APIKey, companyID);
        setUserInfo(userResp.data.data);
        if (userResp.data.testMode != null) {
          setTestMode(userResp.data.testMode === true);
        }

    

        if (contractorID != null) {
          const contractorResp = await getContractorInfo(
            APIKey,
            companyID,
            contractorID
          );
          setContractorInfo(contractorResp.data.data);
        }

        setLoading(false);
      } catch (error) {
        /* eslint-disable no-console */
        console.log("PuzzlOnboarding Setup Error:", error.message);

        if (error.response) {
          if (error.response.data && error.response.data.message) {
            
            console.log(`Puzzl Request Error: ${error.response.data.message}`);
          } else {
           
            console.log(`Puzzl Request Error: ${error.response.statusText}`);
          }
        }
        /* eslint-enable no-console */

        if (showError) {
          Alert.alert(
            "Error",
            errorMessage,
            [
              {
                text: "OK",
                onPress: () => (onError ? onError(errorMessage) : onCancel()),
              },
            ],
            { cancelable: false }
          );
        } else if (onError) {
          await onError("there was an error");
        } else {
          await onCancel();
        }
      }
    })();
  }, []);


  

  const order = [{"ContractorProfileInfo": profile}, {"CreateAccount":contractor_acct}, {"HelloSign":true}]
  console.log("first")

  const route = order.filter(object => Object.values(object)[0] == true)
  console.log(route)
  // const order = ["FirstScreeen", "SecondScreen", "ThirdScreen", "FourthScreen", "FifthScreen"]
  var index = 0

  function getNextScreen(component:boolean){
    if (component) {
      const nextScreen = route[index]
      index += 1
      // const temp = index + 1
      // setIndex(temp)
      
      return Object.keys(nextScreen)[0]
    }
    return ""
  }


  return (
    <SafeAreaProvider>
      <SafeAreaView style={styles.container} edges={["right", "top", "left"]}>
        <LoadingScreen visible={loading} />
        {!loading && (
          <>
            <ExitDialog
              visible={confirmExit}
              userInfo={userInfo}
              handleHide={() => showConfirmExit(false)}
              handleExit={onCancel}
            />
            {!controlsHidden && (
              <>
                <TestModeBanner visible={testMode} />
                <SafeAreaInsetsContext.Consumer>
                  {(insets) => (
                    <TouchableOpacity
                      onPress={() => showConfirmExit(true)}
                      style={[
                        styles.closeTouch,
                        {
                          top: (insets?.top ?? 44) + 8,
                        },
                      ]}
                    >
                      <View style={styles.closeContainer}>
                        <Image style={styles.closeIcon} source={close_icon} />
                      </View>
                    </TouchableOpacity>
                  )}
                </SafeAreaInsetsContext.Consumer>
              </>
            )}
            <NavigationContainer>
            <ErrorContext.Provider value = {onError}>
              <Stack.Navigator initialRouteName="Intro" headerMode="none">
              
                <Stack.Screen
                  name="Intro"
                  component={Intro}
                  
                  initialParams={{
                    
                    nextScreen:getNextScreen(true),
                    contractorInfo:contractorInfo!,
                    userInfo: userInfo!,
                  }}
                  options={{
                    gestureEnabled: false,
                  }}
                />
                
                {/* <Stack.Screen
                  name="WorkerProfileInfo"
                  component={WorkerProfileInfo}
                  initialParams={{
                    APIKey,
                    companyID,
                    
                    contractorID,
                    userInfo: userInfo!,
                    contractorInfo: contractorInfo!,
                    nextScreen: getNextScreen(profile)
                  }}
                /> */}
                <Stack.Screen
                  name="ContractorProfileInfo"
                  component={ContractorProfileInfo}
                  initialParams={{
                    APIKey,
                    companyID,
                    
                    contractorID,
                    userInfo: userInfo!,
                    contractorInfo: contractorInfo!,
                    nextScreen: getNextScreen(profile)
                  }}
                />
                <Stack.Screen
                  name="CreateAccount"
                  component={CreateAccount}
                  initialParams={{
                    APIKey,
                    companyID,
                  
                    contractorID,
                    userInfo: userInfo!,
                    nextScreen: getNextScreen(contractor_acct),
                    contractorInfo: contractorInfo!,
                    testMode,
                  }}
                />
                {/* <Stack.Screen
                  name="Veriff"
                  initialParams={{
                    APIKey,
                    companyID,
                    employeeID,
                    userInfo: userInfo!,
                  }}
                >
                  {(props) => (
                    <Veriff {...props} setControlsHidden={setControlsHidden} />
                  )}
                </Stack.Screen> */}
                <Stack.Screen
                  name="HelloSign"
                  initialParams={{
                    APIKey,
                    companyID,
                    contractorID,
                    userInfo: userInfo!,
                    contractorInfo: contractorInfo!
                  }}
                  options={{
                    gestureEnabled: false,
                  }}
                >
                  {(props) => (
                    <HelloSign
                      {...props}
                      setControlsHidden={setControlsHidden}
                    />
                  )}
                </Stack.Screen>
                <Stack.Screen
                  name="ThankYou"
                  initialParams={{
                    userInfo: userInfo!,
                  }}
                  options={{
                    gestureEnabled: false,
                  }}
                >
                  {(props) => <ThankYou {...props} onFinished={onFinished} />}
                </Stack.Screen>
              </Stack.Navigator>
              </ErrorContext.Provider>
            </NavigationContainer>
          </>
        )}
      </SafeAreaView>
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  closeTouch: {
    position: "absolute",
    top: 52,
    right: 8,
    zIndex: 4,
  },
  closeContainer: {
    width: 40,
    height: 40,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ffffff",
    borderWidth: 2,
    borderColor: "#cccccc",
    borderRadius: 20,
  },
  closeIcon: {
    resizeMode: "contain",
    width: 18,
    height: 18,
  },
});

export default PuzzlOnboarding;
