import type {
  GetHelloSignResponseType,
  GetUserInfoResponseType,
  
  GetContractorInfoResponseType,
} from "./api";

type PersonalDataType = {

  address: string;
  city: string;
  state: string;
  zip: string;
  first_name: string;
  last_name: string;
  middle_name: string;
  type: "individual" | "business";
  ein?: string;
  business_name?: string;
  ssn?: string;
 
};

export type RootStackParamList = {
  Intro: {
    
    contractorInfo?: GetContractorInfoResponseType["data"];
    nextScreen?: string;
    userInfo?: GetUserInfoResponseType["data"];
  };
  WorkerProfileInfo: {
    APIKey?: string;
    companyID?: string;
    contractorID?: string;
    userInfo?: GetUserInfoResponseType["data"];
    contractorInfo?: GetContractorInfoResponseType["data"];
    nextScreen?: string;
  };
  ContractorProfileInfo: {
    APIKey?: string;
    companyID?: string;
    contractorID?: string;
    userInfo?: GetUserInfoResponseType["data"];
    contractorInfo?: GetContractorInfoResponseType["data"];
    nextScreen?: string;
  };
  CreateAccount: {
    APIKey?: string;
    companyID?: string;
    personalData: PersonalDataType;
    contractorID?: string;
    userInfo?: GetUserInfoResponseType["data"];
    nextScreen?: string;
    contractorInfo?: GetContractorInfoResponseType["data"];
    testMode?: boolean;

  };
  Veriff: {
    APIKey?: string;
    companyID?: string;
    
    userInfo?: GetUserInfoResponseType["data"];
    personalData: PersonalDataType;
    email: string;
  };
  HelloSign: {
    APIKey?: string;
    companyID?: string;
    contractorID?: string;
    userInfo?: GetUserInfoResponseType["data"];
    helloInfo: GetHelloSignResponseType;
    contractorInfo?: GetContractorInfoResponseType["data"];
    personalData: PersonalDataType;
  };
  ThankYou: {
    userInfo?: GetUserInfoResponseType["data"];
  };
};
