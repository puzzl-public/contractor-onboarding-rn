import * as React from "react";
import { StyleSheet, View, Text, TextInput, Image, Alert } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import type { RouteProp } from "@react-navigation/native";
import type { StackNavigationProp } from "@react-navigation/stack";
import ActionButton from "../components/ActionButton";
import Checkbox from "../components/Checkbox";
import { fontFamily } from "../utils/consts";
import { submitContractorProfileInfo } from "../functions/api";
import type { RootStackParamList } from "../types/navigation";
import ErrorContext from "../context/ErrorContext";
const ssnPattern = /^[0-9]{3}-?[0-9]{2}-?[0-9]{4}$/;

type PersonalDataStateType = {
  first_name: string;
  last_name: string;
  middle_name: string;
  address: string;
  city: string;
  
  state: string;
 
  zip: string;
  dob: string;
  ssn:string;
  type: "individual" | "business"
  ein: string;
  business_name: string;
};

interface PropTypes {
  route?: RouteProp<RootStackParamList, "ContractorProfileInfo">;
  navigation?: StackNavigationProp<RootStackParamList, "ContractorProfileInfo">;
  
}

const ContractorProfileInfo: React.FC<PropTypes> = ({ route, navigation }) => {
  const {
   
    APIKey,
    companyID,
    contractorID,
    userInfo,
    contractorInfo,
    nextScreen
  } = route!.params!;
 
  
  

  const [personalData, setPersonalData] = React.useState<PersonalDataStateType>(
    {
      first_name: contractorInfo?.first_name || "",
      last_name: contractorInfo?.last_name || "",
      middle_name: contractorInfo?.middle_name || "",
      address: contractorInfo?.address || "",
      city: contractorInfo?.city || "",
      
      state: contractorInfo?.state || "",
      zip:  contractorInfo?.zip || "",
      dob: contractorInfo?.dob || "",
      ssn: contractorInfo?.ssn || "",
      type: contractorInfo?.type || "individual",
      ein: contractorInfo?.ein || "",
      business_name: contractorInfo?.business_name || ""
    }
  );
  const onError = React.useContext<(error:any) => void | Promise<void>>(ErrorContext)
  const [businessName, setBusinessName] = React.useState<string>(personalData.business_name);
  const [ein, setEin] = React.useState<string>(personalData.ein);
  const [accountType, setAccountType] = React.useState<
  "individual" | "business"
>(personalData.type);
  const ssnTwo = React.useRef<TextInput>(null);
  const ssnThree = React.useRef<TextInput>(null);

  const {
    first_name,
    last_name,
    middle_name,
    address,
    city,
    state,
    zip,
  } = personalData;

    const splitssn = (ssn:string) => {
    const result = []
    result.push(ssn.substring(0, 3))
    result.push(ssn.substring(3, 5))
    result.push(ssn.substring(5, 9))
    console.log(result)
    return result
  }
  const parts = personalData.ssn != "" ? splitssn(personalData.ssn) : ["", "", ""]
  const [ssnParts, setSSNParts] = React.useState<string[]>(parts);
  const updateSSN = (index: number, value: string) => {
    setSSNParts((prevData) => {
      const ssnCopy = [...prevData];
      ssnCopy[index] = value;
      return ssnCopy;
    });

    if (index === 0 && value.length === 3) {
      ssnTwo.current?.focus();
    } else if (index === 1 && value.length === 2) {
      ssnThree.current?.focus();
    }
  };

  const updateField = (fieldName: string, fieldValue: string) =>
    setPersonalData((prevData) => ({
      ...prevData,
      [fieldName]: fieldValue,
    }));

  const validateFields = () => {
    if (
      first_name === "" ||
      last_name === "" ||
      address === "" ||
      city === "" ||
      state === "" ||
      zip === ""
    ) {
      Alert.alert(
        "Error",
        "Please fill in all required fields",
        [
          {
            text: "Ok",
          },
        ],
        { cancelable: false }
      );

      return false;
    }

    if (accountType === "individual" && !ssnPattern.test(ssnParts.join(""))) {
      Alert.alert(
        "Error",
        "SSN format is invalid, please try again",
        [
          {
            text: "Ok",
          },
        ],
        { cancelable: false }
      );

      return false;
    }

    return true;
  };

  const goToNextScreen = async () => {
   
    if (validateFields()) {
      try {
        const response = await submitContractorProfileInfo(APIKey!, {
          companyID: companyID!,
          contractorID: contractorID!,
          address,
          city,
          state,
          zip,
          first_name,
          last_name,
          middle_name: middle_name,
          type: accountType,
          ...(accountType === "business"
            ? {
                ein,
                business_name: businessName,
              }
            : {
                ssn: ssnParts.join(""),
              }),
        });
       
        if (response.data.success) {
          let page = nextScreen! as any 
          navigation!.navigate(page, {personalData})
        } else {
          
          throw new Error("Failed to set profile information!");
          
        }
      } catch (error) {
        onError(error)
        Alert.alert(
          "Error",
          error.message,
          [
            {
              text: "Ok",
            },
          ],
          { cancelable: false }
        );
      }
    }
  };
 
  return (
    <KeyboardAwareScrollView style={styles.scroll}>
      <View style={styles.scrollContainer}>
        {userInfo?.hasLogo && (
          <Image
            source={{
              uri: userInfo.logoUrl!,
            }}
            style={styles.logo}
          />
        )}
        <Text style={styles.header}>Profile Information</Text>
        <View style={styles.section}>
          <View style={styles.singleRow}>
            <TextInput
              style={[styles.inputField, styles.longField]}
              placeholder="First Name"
              value={first_name}
              onChangeText={(text) => updateField("first_name", text)}
            />
            <TextInput
              autoCapitalize="characters"
              style={[styles.inputField, styles.shortField]}
              placeholder="MI"
              maxLength={1}
              value={middle_name}
              onChangeText={(text) => updateField("middle_name", text)}
            />
          </View>
          <TextInput
            style={[styles.inputField, styles.longField]}
            placeholder="Last Name"
            value={last_name}
            onChangeText={(text) => updateField("last_name", text)}
          />
        </View>
        <View style={styles.section}>
          <Text style={styles.section_header}>Choose One</Text>
          <View style={styles.checkboxContainer}>
            <Checkbox
              checked={accountType === "individual"}
              onPress={() => setAccountType("individual")}
            />
            <Text style={styles.label}>Individual</Text>
          </View>
          <View style={styles.checkboxContainer}>
            <Checkbox
              checked={accountType === "business"}
              onPress={() => setAccountType("business")}
            />
            <Text style={styles.label}>Business</Text>
          </View>
        </View>
        {accountType === "individual" ? (
          <View style={styles.section}>
            <Text style={styles.section_header}>Social Security Number</Text>
            <View style={styles.singleRow}>
              <TextInput
                keyboardType="number-pad"
                style={[styles.inputField, styles.shortField]}
                value={ssnParts[0]}
                maxLength={3}
                onChangeText={(text) => updateSSN(0, text)}
              />
              <TextInput
                ref={ssnTwo}
                keyboardType="number-pad"
                style={[styles.inputField, styles.shortField]}
                value={ssnParts[1]}
                maxLength={2}
                onChangeText={(text) => updateSSN(1, text)}
              />
              <TextInput
                ref={ssnThree}
                keyboardType="number-pad"
                style={[styles.inputField, styles.shortField]}
                value={ssnParts[2]}
                maxLength={4}
                onChangeText={(text) => updateSSN(2, text)}
              />
            </View>
          </View>
        ) : (
          <>
            <View style={styles.section}>
              <TextInput
                style={[styles.inputField, styles.longField]}
                placeholder="Business Name"
                value={businessName}
                onChangeText={(text) => setBusinessName(text)}
              />
              <TextInput
                style={[styles.inputField, styles.longField]}
                placeholder="EIN"
                value={ein}
                onChangeText={(text) => setEin(text)}
              />
            </View>
          </>
        )}
        <View style={styles.section}>
          <Text style={styles.section_header}>Address</Text>
          <TextInput
            style={[styles.inputField, styles.longField]}
            placeholder="Address"
            value={address}
            onChangeText={(text) => updateField("address", text)}
          />
          <View style={styles.singleRow}>
            <TextInput
              style={[styles.inputField, styles.longField]}
              value={city}
              placeholder="City"
              onChangeText={(text) => updateField("city", text)}
            />
            <TextInput
              style={[styles.inputField, styles.shortField]}
              value={state}
              placeholder="State"
              maxLength={2}
              autoCapitalize="characters"
              numberOfLines={1}
              onChangeText={(text) => updateField("state", text)}
            />
            <TextInput
              keyboardType="number-pad"
              style={[styles.inputField, styles.mediumField]}
              value={zip}
              placeholder="Zip"
              maxLength={5}
              onChangeText={(text) => updateField("zip", text)}
            />
          </View>
        </View>
        <View style={styles.footer}>
          <ActionButton onPress={goToNextScreen}>Create Account</ActionButton>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  scroll: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  scrollContainer: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    paddingHorizontal: 20,
    paddingVertical: 32,
    backgroundColor: "#ffffff",
  },
  logo: {
    marginBottom: 0,
    resizeMode: "contain",
    width: 150,
    height: 80,
    alignSelf: "center",
  },
  header: {
    fontSize: 24,
    fontWeight: "700",
    textAlign: "center",
    marginBottom: 16,
    fontFamily,
  },
  subheader: {
    fontSize: 16,
    textAlign: "center",
    fontFamily,
  },
  section: {
    marginTop: 10,
    marginBottom: 10,
  },
  section_header: {
    fontSize: 18,
    fontWeight: "700",
    fontFamily,
  },
  inputField: {
    borderRadius: 5,
    borderColor: "#DADADA",
    borderWidth: 2,
    paddingHorizontal: 4,
    paddingVertical: 8,
    color: "#000000",
    marginTop: 10,
    marginBottom: 10,
    marginRight: 5,
    fontFamily,
    fontSize: 20,
  },
  longField: {
    width: 180,
  },
  mediumField: {
    width: 80,
  },
  shortField: {
    width: 60,
  },
  ssn: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
  },
  datepicker: {
    padding: 5,
    width: 30,
  },
  calendar_icon: {
    padding: 10,
  },
  singleRow: {
    flexDirection: "row",
  },
  footer: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  label: {
    margin: 8,
    fontSize: 20,
    fontFamily,
  },
  checkboxContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    alignSelf: "flex-start",
  },
});

export default ContractorProfileInfo;
