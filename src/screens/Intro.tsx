import * as React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Text,

} from "react-native";
import type { RouteProp } from "@react-navigation/native";
import type { StackNavigationProp } from "@react-navigation/stack";
import { fontFamily } from "../utils/consts";
import type { RootStackParamList } from "../types/navigation";

interface PropTypes {
  route: RouteProp<RootStackParamList, "Intro">;
  navigation: StackNavigationProp<RootStackParamList, "Intro">;
  
}

const Intro: React.FC<PropTypes> = ({ navigation, route}) => {
  const { contractorInfo, nextScreen, userInfo } = route.params;
  const { business_name } = userInfo!;
  const personalData = {
    first_name: contractorInfo?.first_name || "",
    last_name: contractorInfo?.last_name || "",
    middle_initial: "",
    address: "",
    city: "",
    phone_number: "",
    state: "",
    zip: "",
  }
  const goToNextScreen = () => {
    let page = nextScreen! as any 
    
    
   
    navigation.navigate(page, {personalData});
  };

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        {userInfo?.hasLogo && (
          <Image
            source={{
              uri: userInfo.logoUrl!,
            }}
            style={styles.logo}
          />
        )}
        <Text style={styles.header}>
          
           Welcome to {business_name}'s payment onboarding process.
         
        </Text>
        <Text style={styles.intro_text}>
          This process should only take 2 minutes.
        </Text>
        <Text style={styles.instruction_header}>Steps:</Text>
        
          <Text style={styles.list}>
            1. Complete profile information{"\n"}
            2. Create {business_name} Contractor Account{"\n"}
            3. Fill in remaining fields of paperwork
          </Text>
        
      </View>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.button} onPress={goToNextScreen}>
          <Text style={styles.buttonText}>Start</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    backgroundColor: "#ffffff",
    paddingVertical: 32,
  },
  content: {
    alignItems: "center",
    justifyContent: "flex-start",
    paddingHorizontal: 20,
  },
  logo: {
    marginBottom: 10,
    resizeMode: "contain",
    width: 150,
    height: 80,
    alignSelf: "center",
  },
  header: {
    fontSize: 24,
    textAlign: "center",
    marginBottom: 32,
    fontFamily,
    lineHeight: 30,
  },
  intro_text: {
    fontSize: 24,
    textAlign: "center",
    marginBottom: 16,
    fontFamily,
    lineHeight: 30,
  },
  instruction_header: {
    width: "100%",
    fontSize: 22,
    textAlign: "left",
    marginTop: 50,
    marginBottom: 15,
    fontFamily,
  },
  list: {
    fontSize: 20,
    textAlign: "left",
    fontFamily,
    lineHeight: 30,
  },
  button: {
    marginTop: 70,
    backgroundColor: "#0E64DC",
    borderRadius: 5,
    paddingTop: 15,
    paddingBottom: 15,
    paddingRight: 45,
    paddingLeft: 45,
  },
  buttonText: {
    color: "#fff",
    fontSize: 22,
    fontFamily,
  },
  footer: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "center",
    paddingRight: 45,
    paddingLeft: 45,
  },
  footerText: {
    color: "#979797",
    fontFamily,
    lineHeight: 20,
    marginTop: 45,
    textAlign: "center",
    maxWidth: 250,
  },
  link: {
    textDecorationLine: "underline",
  },
});

export default Intro;
