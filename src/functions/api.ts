import axios from "axios";
import type {
  GetHelloSignRequestType,
  GetHelloSignResponseType,
  GetSSCardPutURLResponseType,
  GetUserInfoResponseType,
  GetVeriffRequestType,
  GetVeriffResponseType,
  GetWorkerInfoResponseType,
  GetContractorInfoResponseType,
  SubmitAccountInfoRequestType,
  SubmitAccountInfoResponseType,
  SubmitProfileInfoRequestType,
  SubmitProfileInfoResponseType,
  SubmitContractorProfileInfoRequestType,
  SubmitContractorProfileInfoResponseType,
  SubmitWorkerSSCardRequestType,
  SubmitWorkerVerificationRequestType,
  SubmitWorkerVerificationResponseType,
  SubmitContractorPaperworkRequestType,
  SubmitContractorPaperworkResponseType,
} from "../types/api";

const apiBase = "https://api.joinpuzzl.com";

axios.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    if (error.code === "ECONNABORTED") {
      /* eslint-disable-next-line no-console */
      console.log("PuzzlOnboarding Connection Error: ", error);

      return Promise.reject(
        new Error("Unable to onboard at this time, please try again later!")
      );
    }

    return Promise.reject(error);
  }
);

export const getUserInfo = (APIKey: string, companyID: string) =>
  axios.get<GetUserInfoResponseType>(apiBase + "/mobile/getUserInfo", {
    headers: {
      authorization: `Bearer ${APIKey}`,
    },
    timeout: 8000,
    params: {
      companyID,
    },
  });

export const getWorkerInfo = (
  APIKey: string,
  companyID: string,
  employeeID: string
) =>
  axios.get<GetWorkerInfoResponseType>(apiBase + "/mobile/getWorkerInfo", {
    headers: {
      authorization: `Bearer ${APIKey}`,
    },
    timeout: 8000,
    params: {
      companyID,
      employeeID,
    },
  });

export const getContractorInfo = (
  APIKey: string,
  companyID: string,
  contractorID: string
) =>
  axios.get<GetContractorInfoResponseType>(
    apiBase + "/mobile/getContractorInfo",
    {
      headers: {
        authorization: `Bearer ${APIKey}`,
      },
      timeout: 8000,
      params: {
        companyID,
        contractorID,
      },
    }
  );

export const submitProfileInfo = (
  APIKey: string,
  data: SubmitProfileInfoRequestType
) =>
  axios.post<SubmitProfileInfoResponseType>(
    apiBase + "/mobile/submitWorkerProfileInfo",
    data,
    {
      headers: {
        authorization: `Bearer ${APIKey}`,
      },
      timeout: 8000,
    }
  );

export const submitContractorProfileInfo = (
  APIKey: string,
  data: SubmitContractorProfileInfoRequestType
) =>
  axios.post<SubmitContractorProfileInfoResponseType>(
    apiBase + "/mobile/submitContractorProfileInfo",
    data,
    {
      headers: {
        authorization: `Bearer ${APIKey}`,
      },
      timeout: 8000,
    }
  );

export const submitAccountInfo = (
  APIKey: string,
  data: SubmitAccountInfoRequestType
) => {
  if (
    (data.employeeID == null && data.contractorID == null) ||
    (data.employeeID != null && data.contractorID != null)
  ) {
    throw new Error("Invalid request format!");
  }

  return axios.post<SubmitAccountInfoResponseType>(
    data.employeeID != null
      ? apiBase + "/mobile/submitWorkerAccountInfo"
      : apiBase + "/mobile/submitContractorAccountInfo",
    data,
    {
      headers: {
        authorization: `Bearer ${APIKey}`,
      },
      timeout: 8000,
    }
  );
};

export const getVeriff = (APIKey: string, data: GetVeriffRequestType) =>
  axios.post<GetVeriffResponseType>(apiBase + "/mobile/getVeriffSetup", data, {
    headers: {
      authorization: `Bearer ${APIKey}`,
    },
    timeout: 8000,
  });

export const submitWorkerVerification = (
  APIKey: string,
  data: SubmitWorkerVerificationRequestType
) =>
  axios.post<SubmitWorkerVerificationResponseType>(
    apiBase + "/mobile/submitWorkerVerification",
    data,
    {
      headers: {
        authorization: `Bearer ${APIKey}`,
      },
      timeout: 8000,
    }
  );

export const submitWorkerSSCard = async (
  APIKey: string,
  data: SubmitWorkerSSCardRequestType
) => {
  const getPutUrlResp = await axios.get<GetSSCardPutURLResponseType>(
    apiBase + "/generate-sscard-put-url",
    {
      params: {
        Key: `${data.email}-sscard`,
        ContentType: "image/png",
      },
      headers: {
        authorization: `Bearer ${APIKey}`,
        "Content-Type": "image/png",
      },
      timeout: 8000,
    }
  );

  if (!getPutUrlResp.data.success) {
    throw new Error("Failed to prepare Social Security Card upload!");
  }

  return axios.put(getPutUrlResp.data.data.putURL, data.image, {
    headers: {
      "Content-Type": "image/png",
    },
    timeout: 15000,
  });
};

export const getHelloSign = (APIKey: string, data: GetHelloSignRequestType) =>
  axios.post<GetHelloSignResponseType>(
    "https://api.joinpuzzl.com/mobile/openContractorPaperwork",
    data,
    {
      headers: {
        authorization: `Bearer ${APIKey}`,
      },
      timeout: 8000,
    }
  );

  export const submitContractorPaperwork = (
    APIKey: string,
    data: SubmitContractorPaperworkRequestType
  ) =>
    axios.post<SubmitContractorPaperworkResponseType>(
      apiBase + "/mobile/submitContractorPaperwork",
      data,
      {
        headers: {
          authorization: `Bearer ${APIKey}`,
        },
        timeout: 8000,
      }
    );

